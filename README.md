This project deals with the training of a model for Abstractive Text Summarization (ATS) for Italian language starting from a pre-trained language model.

#### Install the required packages 

```
pip install -r requirements.txt
```

Working with Python 3.7 (Google Colab), tested with Python 3.9

## Merge the datasets and split them into train/valid/test 

This step is optional if you already have train / valid / test datasets containing two columns only named **news_it** and **summary_it**

```
python scripts/dataset_merger.py [-h] 
                                 [--data_dir]
                                 [--max_number_of_articles]
                                 [--output_dataset]
```

Notes:

  - dataset name to be merged must have the format LG_it_all_selected_articles.csv, where LG = original language (en, es, de...)
  
  - each dataset has the following columns: **news_LG**, **summary_LG**, **news_it** and **summary_it** where LG is the original language

Arguments

  - data_dir: directory in which the datasets to be merged are stored (default: data/raw)
  
  - max_number_of_articles: maximum number of articles in the resulting dataset (default: all)
  
  - output_dataset: path to merged dataset (default: data/merged_dataset.csv)

```
python scripts/dataset_splitter.py [-h] 
                                   [--input_dataset]
                                   [--train_fraction]
                                   [--output_datasets]
```

  - input_dataset: path in which the merged dataset will be stored after merging (default: data/merged_dataset.csv)

  - train_fraction: fraction of the dataset for train / validation / test split (default: 0.7). Validation and test datasets are splitted by half with the examples not used for training. For example, if train_fraction = 0.7, then the validation and test fraction are (1 - 0.7)/2 = 0.15 both.

  - output_datasets: path in which the output csv files will be stored after merging and splitting (default: data/dataset_for_XX.csv, where XX = train, valid, test. If you give a different name, train / valid / test labels will be added to name)")
  
## Merge the Italian datasets and split them into train / valid / test 

```
python scripts/ITA_dataset_creators/ITA_dataset_merger.py [-h] 
                                                          --dataset_dir 
                                                          --common_attribute
                                                          [--output_file]
```
ITA dataset merger

Arguments:

  - dataset_dir: directory in which the italian datasets are stored
  
  - common_attribute: common string shared by the name of the csv files to be merged (for example: "text_and_summary")
  
  - output_file: path to the merged dataset (default: ./data/merged_dataset.csv)

Note: when merging multiple datasets from different sources, that is, different online media, the **name** of the online medium itself is added in a different column. This column will be used for stratified splitting in train / valid / test dataset.

```
python scripts/ITA_dataset_creators/ITA_dataset_splitter.py [-h] 
                                                            [--input_dataset] 
                                                            [--train_fraction]
                                                            [--output_datasets]
```

Dataset train/valid/test splitter

Arguments:

  - input_dataset: path in which the merged dataset will be stored after merging (default:                      data/merged_dataset.csv)
  
  - train_fraction: fraction of the dataset for train / validation / test split (default: 0.7). Validation and test           datasets are splitted by half with the examples not used for training dataset. Stratification for different online media will be applied
  
  - output_datasets: path in which the output csv files will be stored after merging and splitting (default:           data/dataset_for_XX.csv, where XX = train, valid, test. If you give a different name, train / valid / test labels will be added to name)
  
## Data augmentation (optional)

If you want to apply data augmentation to **train data only** you can leverage the script ```scripts/train_data_augmentation.py```. 

You will need to install the spaCy model for italian language with the following command from your shell: 
```python -m spacy download it_core_news_lg```. 

Once the model is installed, run the script as follows:

```
python scripts/train_dataset_augmentation.py [-h] 
                                             --train_dataset 
                                             [--augmented_dataset]
                                             [--number_of_articles] 
                                             [--replicas]  
                                             [--top_fraction]
                                             [--mask_fraction]
```                                  

Arguments:

  - train_dataset: path to train dataset to be augmented (required)
  
  - augmented_dataset: path to augmented dataset (default: data/augmented_train_dataset.csv)
  
  - number_of_articles: number of training articles to be considered to apply augmentation (default: all)
  
  - replicas: number of times the random masking is applied to the same text (default: 5)
  
  - top_fraction: fraction of PageRank-ed sentences that cannot be removed (default: 0.25)
  
  - mask_fraction: fraction of sentences that can be removed (default: 0.25)
  
**Notes**

 - if the total number of sentences of an article is less than the number of sentences to be removed plus the number of sentences that must not be removed, i.e. the top ones, then the text made by these top sentences will be returned as a replica.
 
 - duplicated text obtained after augmentation are dropped and only one instance is kept
 
 - the summary of the augmentation - generated texts is the same of the original one

## Train the model

**Note**: the following instructions have been thought keeping in mind the scripts for train / inference with BART. 

Similar scripts have also been written for train / inference with IT5 (pre-trained T5 model for Italian language). Check the helper, that is, run the script with ```-h, --help``` argument to see the different arguments.

```
python pyfiles/BART_model_training.py [-h] 
                                      [--train_dataset] 
                                      [--valid_dataset]
                                      [--hyperparameters] 
                                      [--last_checkpoint]
                                      [--checkpoints_dir] 
                                      [--additional_epochs]
                                      [--tokenizer] 
                                      [--tokens_size] 
                                      [--token_frequency]
                                      [--best_model]
                                      [--board_writer]

```

**Arguments**

  - train_dataset: path to the csv dataset to be used for training (default: data/global_dataset_train.csv)
  
  - valid_dataset: path to the csv dataset to be used for validation (default: data/global_dataset_valid.csv)
                        
  - hyperparameters: path to the file with training hyperparameters (default: data/training_hyperparameters.json, see further for explanation)
                        
  - last_checkpoint: path to the directory with the last model checkpoint (default: data/models/facebook_bart_large)
                        
  - checkpoints_dir: directory in which the model checkpoints will be stored during training (default: data/checkpoints. The directory will be created if not present)
                        
  - additional_epochs: number of additional epochs, useful when restarting from checkpoint (default: 0)
  
  - tokenizer: path to the directory containing the tokenizer (default: ITABS)
  
  - tokens_size: number of tokens the tokenizer has to be trained (will be ignored if a tokenizer is given, default: 50265)
  
  - token_frequency: minimum frequency two tokens should have to be merged (will be ignored if a tokenizer is given,      default: 20)
  
  - best_model: name of the best model resulting after training (default: best_model. The directory will be created if not present)
  
  - board_writer: directory in which the Tensorboard writer will store the files for training monitor. Datetime in the format DD-MM-YYYY_H-M-S corresponding to the beginning of the training will be added to name (default: data/experiment)
  
  To launch Tensorboard type on terminal (supposing that the folder is data/experiment_01-01-2000_01-03-03):
  
  ```
  tensorboard --logdir data/experiment_01-01-2000_01-02-03
  ```
  
  and, then, open the link.
  
**Model hyperparameters**

 - freeze_encoder: whether or not to train the encoder of BART (false = train the encoder)
 
 - freeze_decoder: whether or not to train the decoder of BART (false = train the decoder)
 
 - epochs: number of training epochs
 
 - learning_rate: learning rate of the optimizer 
 
 - weight_decay: L2 - like regularization parameter correctly implemented for Adam
 
 - restart_period: period of the cosine annealing, that is, the number of epochs after which the learning rate starts again from the initial given value
    
 - batch_size: number of parallel works; recommended to use 2^n batch_size, where n is a positive integer
    
 - early_stopping_patience: number of consecutive epochs after which the training is interrupted if valid loss increases
 
 - early_stopping_threshold: tolerance for comparison of the valid loss
  
**Notes**

 - *facebook/bart-large* will be used as a starting checkpoint if none is given. It will be downloaded in the cache of Huggingface transformer in your machine if it is not already present and saved in the current directory.
 
 - *ITABS* (Italian Tokenizer for ABstractive Summarization) tokenizer, that is, the text-to-tensor converter  will be trained and saved in the abs_text_summarization/ITABS directory if no tokenizer is provided.
 
 - a *training_hyperparameters.json* is already provided in data/ as a default for training. Refer to this file if you want to change the hyperparameters for model training or create a new one.
 
## Inference 

```
python pyfiles/BART_test_inference.py [-h] 
                                      [--test_dataset] 
                                      [--summary_parameters]
                                      [--tokenizer] 
                                      [--number_of_articles] 
                                      [--output]
                                      model
```

**Arguments**

  - model: path to model directory (BART - like)

  - test_dataset: path to test dataset (default: data/global_dataset_test.csv)
  
  - summary_parameters: path to json with parameters for summary generation (default: data/summary_generation_parameters.json)
  
  - tokenizer: path to tokenizer directory (default: ITABS)
  
  - number_of_articles: number of articles to be summarized (default: all)
  
  - output: path to the output file (default: data/test_ds_with_generated_summaries.csv)
  
**Parameters for summary generation**
   
  - max/min_length: maximum/minimum number of tokens of the generated summary
    
  - beam_hypothesis: number of beam hypothesis to be run 
  
  - temperature: temperature of the model (avoid the model to be stuck on most probable words only)
  
  - top_p: the model will consider the minimum number of words whose total probability is >= top_p
 
  - length_penalty: > 1 favours shorter summaries, 1 means no penalty
  
  - repetition_penalty: > 1 penalizes word repetition, 1 means no penalty
  
  - no_repeat_ngram_size: size of the ngrams that must not be repeated. For ex., if = 1, then each unigrams can appear only once

**Notes**

  - a *summary_generation_parameters.json* is already provided in data/ as a default for summary generation. Refer to this file if you want to change the parameters for summary generation or create a new one.