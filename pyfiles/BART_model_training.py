import os
import json

import pandas as pd

from datetime import datetime as d

import torch
from torch.utils.tensorboard import SummaryWriter

from transformers import BartForConditionalGeneration, BartTokenizerFast
from transformers import Trainer, TrainingArguments

from transformers import AdamW

# copy / paste scheduler after optimizer declaration 
#from transformers import get_constant_schedule
#learning_rate_scheduler = get_constant_schedule(optim)

#from torch.optim.lr_scheduler import StepLR
"""
learning_rate_scheduler = StepLR(optim, 
                                 step_size = steps_per_epoch, 
                                 gamma = 1./3,  
                                 verbose = False)
"""

from torch.optim.lr_scheduler import CosineAnnealingWarmRestarts


from transformers import EarlyStoppingCallback
from transformers.integrations import TensorBoardCallback

from classes_and_functions import *

# ------------------ parsing arguments

import argparse

parser = argparse.ArgumentParser(description = """BART model trainer""", epilog = "See you later")

parser.add_argument('--train_dataset',
                    type = str, 
                    default = "data/global_dataset_train.csv",
                    help = "path to the csv dataset to be used for training (default: data/global_dataset_train.csv)")

parser.add_argument('--valid_dataset',
                    type = str, 
                    default = "data/global_dataset_valid.csv",
                    help = "path to the csv dataset to be used for validation (default: data/global_dataset_valid.csv)")

parser.add_argument('--hyperparameters',
                    type = str, 
                    default = "data/training_hyperparameters.json",
                    help = "path to the file with hyperparameters.json (default: data/training_hyperparameters.json)")
                    
parser.add_argument('--last_checkpoint',
                    type = str, 
                    default = "data/models/facebook_bart_large",
                    help = "path to the directory with the last model checkpoint (default: data/models/facebook_bart_large)")

parser.add_argument('--checkpoints_dir',
                    type = str, 
                    default = 'data/checkpoints',
                    help = "directory in which the model checkpoints will be stored during training (default: data/checkpoints)")

parser.add_argument('--tokenizer',
                    type = str, 
                    default = "ITABS_BART",
                    help = "path to the directory containing the tokenizer (default: ITABS_BART)")
                    
parser.add_argument('--tokens_size',
                    type = int, 
                    default = 50265,
                    help = "Number of tokens the tokenizer has to be trained (will be ignored if a tokenizer is given, default: 50265)")
                    
parser.add_argument('--token_frequency',
                    type = int, 
                    default = 20,
                    help = "Minimum frequency two tokens should have to be merged (will be ignored if a tokenizer is given, default: 20)")

parser.add_argument('--best_model',
                    type = str, 
                    default = "best_model",
                    help = "Name of the best model resulting after training (default: best_model)")

parser.add_argument('--board_writer',
                    type = str, 
                    default = "data/experiment",
                    help = "Name of writer for Tensorboard. Datetime in the format DD-MM-YYYY_H-M-S corresponding to the beginning of the training will be added to name (default: data/experiment)")
                    
arguments = parser.parse_args()  

##########################################################################################
##########################################################################################
##########################################################################################

# ------------------ create the writer for Tensorboard

writer = SummaryWriter(f"{arguments.board_writer}_{d.now().strftime('%d-%m-%Y__%H-%M-%S')}")
     
# ------------------ loading the dataset with articles

print()
print("********** Loading the dataset", d.now().strftime("%H:%M / %d-%m-%Y"))

re_name = {'news_it': 'text', 'summary_it': 'REF_summary'}

train = pd.read_csv(arguments.train_dataset, index_col = 0)
train.rename(columns = re_name, inplace = True, errors = 'raise')

valid = pd.read_csv(arguments.valid_dataset, index_col = 0)
valid.rename(columns = re_name, inplace = True, errors = 'raise')

# ------------------ loading json file with hyperparameters

file_with_hyperparameters = arguments.hyperparameters

with open(file_with_hyperparameters, mode = 'r') as filename:   
    hyperparameters = json.load(filename)  
        
# ------------------ loading the tokenizer

tokenizer_name = arguments.tokenizer

print()
print(f"********** Loading tokenizer from directory {tokenizer_name}", d.now().strftime("%H:%M / %d-%m-%Y"))

try:
    assert tokenizer_name is not None
    it_tokenizer = BartTokenizerFast.from_pretrained(tokenizer_name)
    
except Exception as E:
    
    print()
    print("Pre - trained tokenizer not found or given --> training a new one")
        
    from tokenizers import ByteLevelBPETokenizer

    all_texts = train['text'].values
    
    with open('all_texts.txt', 'w') as filehandle:
        for listitem in all_texts:
            filehandle.write('%s\n' % listitem)
    
    it_tokenizer = ByteLevelBPETokenizer()
    
    special_tokens = ['<s>','<pad>','</s>','<unk>','<mask>','<bos>','<eos>']
    
    it_tokenizer.train(files = 'all_texts.txt',
                       vocab_size = arguments.tokens_size,
                       min_frequency = arguments.token_frequency,
                       special_tokens = special_tokens)
                    
    tokenizer_name = "ITABS" # Italian Tokenizer for ABstractive Summarization
    
    try:
        os.mkdir(tokenizer_name) 
    except FileExistsError:
        pass
        
    print()
    print(f"INFO: created tokenizer at {tokenizer_name}")
    
    it_tokenizer.save_model(tokenizer_name)
    it_tokenizer = BartTokenizerFast.from_pretrained(tokenizer_name)

# ------------------ train / validation texts & summaries

print()
print(f"********** Train / Validation tokenization", d.now().strftime("%H:%M / %d-%m-%Y"))

train_texts, train_summaries = list(train['text']), list(train['REF_summary'])
valid_texts, valid_summaries = list(valid['text']), list(valid['REF_summary'])

train_dataset = tokenize_data(it_tokenizer, train_texts, train_summaries)   
valid_dataset = tokenize_data(it_tokenizer, valid_texts, valid_summaries)  

# ------------------ loading the model checkpoint

torch_device = 'cuda' if torch.cuda.is_available() else 'cpu'
checkpoint_name = arguments.last_checkpoint 

print()
print(f"********** Loading model from checkpoint {checkpoint_name}", d.now().strftime("%H:%M / %d-%m-%Y"))

try:
    assert checkpoint_name is not None
    bart = BartForConditionalGeneration.from_pretrained(checkpoint_name).to(torch_device)
    
except Exception as E:
    
    print()
    print(f"Checkpoint {checkpoint_name} not found - loading pre-trained Hugging Face model from checkpoint facebook/bart-large")
    
    try:
        # will work if already downloaded
        bart = BartForConditionalGeneration.from_pretrained('facebook_bart_large').to(torch_device)  
        
    except Exception as E:
        
        # otherwise, it will be downloaded and saved for future reuse
        bart = BartForConditionalGeneration.from_pretrained('facebook/bart-large').to(torch_device) 
        bart.save_pretrained("facebook_bart_large")

freeze_encoder = hyperparameters["freeze_encoder"]
freeze_decoder = hyperparameters["freeze_decoder"]

if freeze_encoder:
        for param in bart.model.encoder.parameters():
            param.requires_grad = False

if freeze_decoder:
    for param in bart.model.decoder.parameters():
        param.requires_grad = False
        
# ------------------ training the model

checkpoints_dir = arguments.checkpoints_dir

num_of_epochs = hyperparameters["epochs"]
weight_decay = hyperparameters["weight_decay"]
restart_period = hyperparameters["restart_period"]
learning_rate = hyperparameters["learning_rate"]
    
batch_size = hyperparameters["batch_size"]   
    
early_stopping_patience  = hyperparameters["early_stopping_patience"]
early_stopping_threshold = hyperparameters["early_stopping_threshold"]

print()
print(f"********** Start model training", d.now().strftime("%H:%M / %d-%m-%Y"))

training_arguments = TrainingArguments(output_dir = checkpoints_dir,    
                                       num_train_epochs = num_of_epochs,
                                       per_device_train_batch_size = batch_size,        
                                       per_device_eval_batch_size = batch_size,        
                                       save_strategy = 'epoch',             
                                       evaluation_strategy = 'epoch',          
                                       weight_decay = weight_decay, 
                                       logging_strategy = 'epoch',
                                       load_best_model_at_end = True,
                                       save_total_limit = early_stopping_patience,
                                       learning_rate = learning_rate)

optim = AdamW(bart.parameters(), 
              lr = learning_rate, 
              weight_decay = weight_decay, 
              betas = (0.9, 0.999))

steps_per_epoch = train.shape[0]/batch_size

period = int(restart_period*steps_per_epoch)
learning_rate_scheduler = CosineAnnealingWarmRestarts(optim,
                                                      T_0 = period, 
                                                      T_mult = 1, 
                                                      eta_min = 0, 
                                                      last_epoch = - 1, 
                                                      verbose = False)
    
trainer = Trainer(model = bart,  
                  args = training_arguments, 
                  train_dataset = train_dataset,  
                  eval_dataset = valid_dataset,   
                  tokenizer = it_tokenizer,
                  callbacks = [EarlyStoppingCallback(early_stopping_patience  = early_stopping_patience,                        
                                                     early_stopping_threshold = early_stopping_threshold), 
                              TensorBoardCallback(writer)],
                  optimizers = (optim, learning_rate_scheduler))

print()
print("********** Training start", d.now().strftime("%H:%M / %d-%m-%Y"))

try:
    output = trainer.train(resume_from_checkpoint = checkpoint_name)
    
except Exception as E:
    output = trainer.train()
    
# ------------------ finally, save the model and the hyperparameters

best_model = arguments.best_model
trainer.save_model(best_model)

print()
print("********** Training end", d.now().strftime("%H:%M / %d-%m-%Y"))
print()

with open(f"{best_model}/new_{file_with_hyperparameters}", 'w') as fp:
    json.dump(hyperparameters, fp)

