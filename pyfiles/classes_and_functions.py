from torch.utils.data import Dataset

# ------------------ definition of classes

#### Abstractive Text Summarization (ATS)

class ATS_Dataset(Dataset):
    
    def __init__(self, encodings, labels):
    
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        
        item = {key: torch.tensor(val[idx]) 
                for key, val in self.encodings.items()}
        
        item['labels'] = torch.tensor(self.labels['input_ids'][idx]) 
        
        return item

    def __len__(self):
        
        return len(self.labels['input_ids'])  

# ------------------ definition of functions

#### tokenization

max_length = 512
truncation = True
padding    = True

def tokenize_data(tokenizer, texts, labels):

    encodings = tokenizer(texts, 
                          max_length = max_length, 
                          truncation = truncation, 
                          padding = padding)
    
    decodings = tokenizer(labels,
                          max_length = max_length, 
                          truncation = truncation, 
                          padding = padding)
    
    dataset_tokenized = ATS_Dataset(encodings, decodings)

    return dataset_tokenized