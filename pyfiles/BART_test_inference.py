from transformers import BartTokenizerFast, BartForConditionalGeneration

import json
import pandas as pd

from datetime import datetime as d

# ------------------ parsing arguments

import argparse

parser = argparse.ArgumentParser(description = """Summary generation for test articles""", epilog = "See you later")

parser.add_argument('model',
                    type = str, 
                    default = None,
                    help = "path to model directory (BART - like)")

parser.add_argument('--test_dataset',
                    type = str, 
                    default = "data/global_dataset_test.csv",
                    help = "path to test_dataset (default: data/global_dataset_test.csv)")

parser.add_argument('--summary_parameters',
                    type = str, 
                    default = "data/summary_generation_parameters.json",
                    help = "path to json with parameters for summary generation (default: data/summary_generation_parameters.json)")

parser.add_argument('--tokenizer',
                    type = str, 
                    default = "ITABS",
                    help = "path to tokenizer directory (default: ITABS)")
                    
parser.add_argument('--number_of_articles',
                    type = int, 
                    default = -1,
                    help = "number of articles to be summarized (default: all)")
                    
parser.add_argument('--output',
                    type = str, 
                    default = "data/test_ds_with_generated_summaries.csv",
                    help = "Name of the output file (default: data/test_ds_with_generated_summaries.csv)")

arguments = parser.parse_args()

# ------------------ loading test dataset 

test_dataset = arguments.test_dataset
n_articles = arguments.number_of_articles

print()
print(f"********** Loading test dataset ({n_articles} articles)", d.now().strftime("%H:%M / %d-%m-%Y"))

test_articles = pd.read_csv(test_dataset, index_col = 0, nrows = n_articles)

# ------------------ loading model and tokenizer

print()
print(f"********** Loading model and tokenizer", d.now().strftime("%H:%M / %d-%m-%Y"))

model_name = arguments.model
token_name = arguments.tokenizer

ats_model = BartForConditionalGeneration.from_pretrained(model_name)
ats_token = BartTokenizerFast.from_pretrained(token_name)

# ------------------ loading parameters for summary generation

print()
print(f"********** Loading parameters for summary generation", d.now().strftime("%H:%M / %d-%m-%Y"))

parameters_for_ats_generation = arguments.summary_parameters

with open(parameters_for_ats_generation, mode = 'r') as filename:   
    ats_parameters = json.load(filename) 
    
# ------------------ definition of the function generating the summary of each text
    
def summarize(tokenizer, model, text):

    input_ = tokenizer.encode(text, 
                              return_tensors = "pt", 
                              max_length = 512,
                              truncation = True)
    
    output_ = model.generate(input_, 
                             max_length = ats_parameters["max_length"],
                             min_length = ats_parameters["min_length"],                              
                             num_beams = ats_parameters["beam_hypothesis"],
                             temperature = ats_parameters["temperature"],
                             top_p = ats_parameters["top_p"],                             
                             length_penalty = ats_parameters["length_penalty"],
                             repetition_penalty = ats_parameters["repetition_penalty"],
                             no_repeat_ngram_size = ats_parameters["no_repeat_ngram_size"],
                             do_sample = True,
                             early_stopping = True)
    
    summary = tokenizer.decode(output_[0], skip_special_tokens = True)

    return summary

# ------------------ generating the summaries of the test dataset

print()
print(f"********** Generating the summaries of the test dataset", d.now().strftime("%H:%M / %d-%m-%Y"))
    
test_articles['ATS_summary'] = test_articles['news_it'].apply(lambda t: summarize(tokenizer = ats_tokenizer,
                                                                                  model = ats_model,
                                                                                  text = t))

# ------------------ save the test dataset with the model - generated summaries

print()
print(f"********** Save the test dataset with the model - generated summaries", d.now().strftime("%H:%M / %d-%m-%Y"))

test_articles.to_csv(arguments.output)    

