import re
import gc

import nltk
nltk.download('stopwords', quiet = True)
nltk.download('punkt', quiet = True)
stop_words = set(nltk.corpus.stopwords.words('italian')) 
articles = {'il', 'la', 'lo', 'i', 'gli', 'le', 'un', 'una', 'uno', 'el', 'the'}

import spacy
nlp = spacy.load("it_core_news_lg")

import numpy as np

# --------------------------

def cleaner(text, min_len, rm_stopwords = True, rm_punctuation = True, rm_singlechars = True, rm_whitespaces = True):
    
    """All-in-one function to remove (rm) parts of the text"""
    
    cleaned_text = text
    
    if rm_punctuation:
        cleaned_text = re.sub(r'[^\w\s]',' ', cleaned_text)
        
    if rm_stopwords:
        cleaned_text = ' '.join([word for word in cleaned_text.split() 
                                 if word.lower() not in stop_words and word.lower() not in articles])
       
    if rm_singlechars:
        cleaned_text = ' '.join([w for w in cleaned_text.split() 
                                 if len(w) >= min_len or 
                                 (len(w) < min_len and ((w in ['e', '&']) or 
                                                        (w.lower() in articles) or 
                                                        (w.lower() == 'tv') or
                                                        (w.isnumeric())))])     
    if rm_whitespaces:
        cleaned_text = re.sub(' +', ' ', cleaned_text)
        
    return cleaned_text

def complete_clean(text, min_len):
    
    """Clean the text from everything not needed"""
    
    return cleaner(text, min_len, 
                   rm_stopwords = True, 
                   rm_punctuation = True, 
                   rm_singlechars = True, 
                   rm_whitespaces = True)

# --------------------------

def build_similarity_matrix(sentences, min_len):
    
    d_sents = {i : nlp(complete_clean(s, min_len)) for i, s in enumerate(sentences)}
    
    similarity_matrix = np.zeros((len(sentences),len(sentences)))
    
    for idx1 in range(len(sentences)):
        for idx2 in range(len(sentences)):     
            if idx1 > idx2:
                similarity_matrix[idx1][idx2] = d_sents[idx1].similarity(d_sents[idx2])
                
    return similarity_matrix