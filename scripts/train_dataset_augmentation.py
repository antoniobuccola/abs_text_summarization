import gc
import re

import nltk
import spacy
nlp = spacy.load("it_core_news_lg")

import pandas as pd
import numpy as np
import networkx as nx

from datetime import datetime as ddd
from random import randint

from common_functions import *

# -------- function definition - BEGIN

def find_indexes_of_top_sentences(text, min_len, top_f: float):
    
    assert top_f < 1, "Fraction of most important sentences must be less than 1"
    
    sentences = nltk.sent_tokenize(text)
    
    similarity_matrix = build_similarity_matrix(sentences, min_len)      
    sentence_similarity_graph = nx.from_numpy_array(similarity_matrix)
    
    top_n = int(top_f*len(sentences))
    
    try:
        scores = nx.pagerank(sentence_similarity_graph, max_iter = 1000, tol = 1e-3, alpha = 0.25)      
    except Exception as e:
        # return indexes of the first sentences of the article
        return list(range(top_n))
    
    ranked_indexes = sorted(((scores[i], i) for i, _ in enumerate(sentences)),reverse = True)
   
    del similarity_matrix
    del sentence_similarity_graph
    
    gc.collect()
    
    return [i for score, i in ranked_indexes[:top_n]] # indexes of the first top_n sentences

# ---------------------------------

def sentence_masking(text, top_indexes, mask_fraction):
    
    sentences = nltk.sent_tokenize(text)
    
    n_sents = len(sentences)
    sent_indexes = list(range(n_sents))
    
    n_mask = int(mask_fraction*n_sents)
    
    if n_sents - n_mask < len(top_indexes):        
        n_mask = -1
        sent_indexes = top_indexes
    
    while n_mask > 0:
        
        i = randint(0, n_sents-1)
        
        if i in top_indexes:  continue      
        else:           
            try:                
                sent_indexes.remove(i) 
                n_mask -= 1
            except Exception as E:                
                # the random extraction got the same number (at least) twice
                # no problem, just skip
                continue
    
    unmasked_text = ""
    
    for i in sent_indexes: 
        unmasked_text += sentences[i] + " "
    
    return unmasked_text[:-1]

# -------- function definition - END

import argparse

parser = argparse.ArgumentParser(description = """Data Augmentation through sentence masking """, epilog = "See you later")

parser.add_argument('--train_dataset',
                    type = str, 
                    default = None,
                    help = "Path to train dataset to be augmented (required)",
                    required = True)

parser.add_argument('--augmented_dataset',
                    type = str, 
                    default = "data/augmented_train_dataset.csv",
                    help = "Path to augmented dataset (default: data/augmented_train_dataset.csv)")

parser.add_argument('--number_of_articles',
                    type = int, 
                    default = -1,
                    help = "Number of training articles to be considered to apply augmentation (default: all)")

parser.add_argument('--replicas',
                    type = int, 
                    default = 5,
                    help = "Number of times the random masking is applied to the same text (default: 5)")

parser.add_argument('--top_fraction',
                    type = float, 
                    default = 0.25,
                    help = "Fraction of PageRank-ed sentences that cannot be removed  (default: 0.25)")

parser.add_argument('--mask_fraction',
                    type = float, 
                    default = 0.25,
                    help = "Fraction of sentences that can be removed (default: 0.25)")

arguments = parser.parse_args()

# -------- load the original train dataset

print()
print(f"Loading dataset @ ", ddd.now().strftime("%H:%M:%S / %d-%m-%Y"))

df = pd.read_csv(arguments.train_dataset, index_col = 0)[:arguments.number_of_articles]
assert list(df.columns) == ['news_it', 'summary_it'], "Input dataset must have two columns only: news_it and summary_it"

# -------- apply data augmentation

# fraction of sentencet that must NOT be masked
top_frac = arguments.top_fraction

# fraction of sentences that can be masked
mask_frac = arguments.mask_fraction

# apply masking to the same text more than once; duplicate masked texts will have to be removed
num_of_replicas = arguments.replicas

augm_dfs = []

n_articles = df.shape[0]

for i, (text, summary) in enumerate(df[["news_it", "summary_it"]].values):
    
    print()
    print(f"Sentence masking {i+1} / {n_articles} @ ", ddd.now().strftime("%H:%M:%S / %d-%m-%Y"))
    
    augm_ds = pd.DataFrame(columns = ["news_it", "summary_it"], index = list(range(0,num_of_replicas)))
    
    # find indexes of the sentences that must NOT be masked
    top_indexes = find_indexes_of_top_sentences(text, min_len = 2, top_f = top_frac)
    
    k = 0
    while k < num_of_replicas:
        
        masked_text = sentence_masking(text, top_indexes, mask_fraction = mask_frac)
        augm_ds.loc[k]["news_it"] = masked_text
        augm_ds.loc[k]["summary_it"] = summary
                
        k += 1
        
    augm_dfs.append(augm_ds)
    
print()
print(f"Removing duplicates and saving the augmented dataset @ ", ddd.now().strftime("%H:%M:%S / %d-%m-%Y"))    

augm_df = pd.concat(augm_dfs).reset_index(drop = True)

# remove duplicate "news_it" after masking
augm_df.drop_duplicates(subset = "news_it", keep = "first", inplace = True)

# append the augmented dataset to the original one and save all
final_df = df.append(augm_df, ignore_index = True)
final_df.to_csv(arguments.augmented_dataset)