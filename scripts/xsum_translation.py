import warnings
warnings.filterwarnings("ignore")

import pandas as pd

from time import time, sleep
from datasets import load_dataset
from googletrans import Translator

tr = Translator()

def en_it_translator(source_text):
    
    sleep(0.4)
    
    try:
        trans_text = tr.translate(source_text, src = 'en', dest = 'it').text
        return trans_text
    
    except Exception as E:
        
        print()
        print(source_text)
        print()
        print(E)
        
        return " "

import argparse   

parser = argparse.ArgumentParser(description = """XSum translation with Google Translate Python API""", epilog = "See you later")

parser.add_argument('output', 
                    type = str, 
                    default = None,  
                    help = "path/to/output.csv")

parser.add_argument('--max_articles', 
                    type = int, 
                    default = -1,  
                    help = "Number of articles to be translated (default: all)")

parser.add_argument('--chunk_size', 
                    type = int, 
                    default = 5000,  
                    help = "Number of articles to be saved as a chunk (default: 5000)")

args = parser.parse_args()

output = args.output
max_articles = args.max_articles

dataset = load_dataset("xsum")

news = []
sums = []

for label in ["train", "validation", "test"]: # the splits the dataset is composed of
    
    for dict_ in dataset[label]:   
        
        news.append(dict_['document'])
        sums.append(dict_['summary'])
        
df = pd.DataFrame(list(zip(news, sums)), columns = ['news_en', 'summary_en'])

df['len_news'] = df['news_en'].apply(len)
df['len_summary'] = df['summary_en'].apply(len)

mask_news = (df['len_news'] >= 1000) & (df['len_news'] <= 4000)
mask_sums = (df['len_summary'] >= 100) & (df['len_summary'] <= 400)

sample = df[mask_news & mask_sums][:max_articles]
sample['ratio'] = sample['len_summary']/sample['len_news']

chunk_size = args.chunk_size
for index, chunk in enumerate(np.array_split(sample, chunk_size)):
    
    print()
    print(f"********** Processing chunk {index+1}, size {len(chunk)} @ ", d.now().strftime("%H:%M / %d-%m-%Y"))
    print()
    
    chunk['news_it'] = chunk[f'news_{lang}'].apply(lambda text: xx_it_translator(source_lang = lang, source_text = text))
    chunk['summary_it'] = chunk[f'summary_{lang}'].apply(lambda text: xx_it_translator(source_lang = lang, source_text = text))
    
    chunk_name = f"xlsum_en_it_chunk_{index}_{chunk_size}_articles.csv"
    chunk.to_csv(f"data/chunked_translation/{chunk_name}")