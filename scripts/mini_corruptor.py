import pandas as pd
import pandas as pd
import numpy as np
import networkx as nx

from common_functions import *

# --------------------------

def generate_summary(sentences, min_len, top_n):
    
    similarity_matrix = build_similarity_matrix(sentences, min_len)      
    
    sentence_similarity_graph = nx.from_numpy_array(similarity_matrix)
    
    try:
        scores = nx.pagerank(sentence_similarity_graph, max_iter = 1000, tol = 1e-3, alpha = 0.25)
    except Exception as e:
        return " ".join(sentences[:top_n])      
    
    ranked_sentences = sorted(((scores[i],s) for i,s in enumerate(sentences)),reverse=True)
   
    summarize_text = []
    for i in range(min(top_n, len(sentences))):
        summarize_text.append(ranked_sentences[i][1])
        
    del similarity_matrix
    del sentence_similarity_graph
    del ranked_sentences
    
    gc.collect()
    
    return " ".join(summarize_text)

# --------------------------

import argparse

parser = argparse.ArgumentParser(description = """Blasting News "data corruptor" """, epilog = "See you later")

parser.add_argument('--number_of_articles',
                    type = int, 
                    default = None,
                    help = "Number of articles to be taken for corruption (required)",
                    required = True)
                    
parser.add_argument('--dataset',
                    type = str, 
                    default = None,
                    help = "Path to dataset of Blasting News (required)",
                    required = True) 
                    
parser.add_argument('--top_n',
                    type = int, 
                    default = 2,
                    help = "Number of most important sentences (default: 2)")                    
                    
parser.add_argument('--output',
                    type = str, 
                    default = "BN_corruptor.csv",
                    help = "Path to output corruptor")                      
                    
arguments = parser.parse_args() 

print("Loading dataset")                                    

df = pd.read_csv(arguments.dataset, index_col = 0)

print(df.shape)

info = ["clean_body", "news_title", "news_subtitle"]
sample = df[~df['news_title'].str.contains("(?i)oroscopo")][info][:arguments.number_of_articles] 

print(sample.shape)
sample["sentences"] = sample['clean_body'].apply(lambda text: nltk.sent_tokenize(text))

print("Generating ETSummaries")
sample["summary_it"] = sample["news_title"] + ". " + \
                       sample["news_subtitle"] + " " + \
                       sample["sentences"].apply(lambda sents : generate_summary(sents, min_len = 2, top_n = arguments.top_n))

print("Saving")
sample = sample.rename(columns = {"clean_body" : "news_it"})
sample = sample[["news_it", "summary_it"]].reset_index(drop = True)
print(sample.shape)
sample.to_csv(arguments.output)
