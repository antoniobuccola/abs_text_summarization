import glob
import pandas as pd

import argparse

parser = argparse.ArgumentParser(description = """Dataset merger""", epilog = "See you later")

parser.add_argument('--data_dir',
                    type = str, 
                    default = "data/raw",
                    help = "Directory in which the datasets to be merged are stored (default: data/raw)")

parser.add_argument('--max_number_of_articles',
                    type = int, 
                    default = -1,
                    help = "Maximum number of articles in the resulting dataset (default: all)")

parser.add_argument('--output_dataset',
                    type = str, 
                    default = "data/merged_dataset.csv",
                    help = "Path in which the merged dataset will be stored after merging (default: data/merged_dataset.csv)")

arguments = parser.parse_args()

# ------------------ getting all the file after translation

list_of_files = glob.glob(f"{arguments.data_dir}/*_it_all_selected_articles.csv")

# ------------------ concatenating the found datasets into a single one

data = []

for f in list_of_files:
    
    ds = pd.read_csv(f)
    data.append(ds)
    
all_articles = pd.concat(data).drop(columns = 'Unnamed: 0').reset_index(drop = True)
    
# ------------------ data cleaning

mask_text = all_articles['news_it'] != " "
mask_sum  = all_articles['summary_it'] != " "

articles = all_articles[mask_text & mask_sum][:arguments.max_number_of_articles]

# ------------------ saving the merged dataset

articles.to_csv(arguments.output_dataset)