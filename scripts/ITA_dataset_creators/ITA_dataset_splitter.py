import pandas as pd

from sklearn.model_selection import train_test_split

import argparse

parser = argparse.ArgumentParser(description = """Dataset train/valid/test splitter""", epilog = "See you later")

parser.add_argument('--input_dataset',
                    type = str, 
                    default = "data/merged_dataset.csv",
                    help = "Path in which the merged dataset will be stored after merging (default: data/merged_dataset.csv)")

parser.add_argument('--train_fraction',
                    type = float, 
                    default = 0.7,
                    help = "Fraction of the dataset for train / validation / test split (default: 0.7). Validation and test datasets are splitted by half with the examples not used for training dataset. Stratification for different media will be applied")

parser.add_argument('--output_datasets',
                    type = str, 
                    default = "data/dataset_for_XX.csv",
                    help = "Path in which the output csv files will be stored after merging and splitting (default: data/dataset_for_XX.csv, where XX = train, valid, test. If you give a different name, train / valid / test labels will be added to name)")

arguments = parser.parse_args()

# ------------------ small function to check the ratios in a dataset

def print_ratios(ds, name: str):
    print()
    print(f"{name}: number of articles: {ds.shape[0]}")
    print(ds["name"].value_counts()/ds.shape[0]*100)

# ------------------ load the merged dataset and print its composition

df = pd.read_csv(arguments.input_dataset, index_col = 0)

# train fraction will be user later for train / valid / test split
# if it is not correct, however, there is no need in continuing

train_fraction = arguments.train_fraction
assert train_fraction < 1, "Train fraction must be less than 1"

# ------------------ stratified train / validation / test split

X_train, X_valid_test, y_train, y_valid_test = train_test_split(df["news_it"], 
                                                                df["name"],
                                                                stratify = df["name"], 
                                                                train_size = train_fraction,
                                                                random_state = 42)


train_df = df[df["news_it"].isin(X_train)]

valid_test_df = df[~df["news_it"].isin(X_train)]

X_valid, X_test, y_valid, y_test = train_test_split(valid_test_df["news_it"],
                                                    valid_test_df["name"],
                                                    stratify = valid_test_df["name"], 
                                                    train_size = 0.5,
                                                    random_state = 42)

valid_df = df[df["news_it"].isin(X_valid)]
test_df = df[df["news_it"].isin(X_test)]

print_ratios(df, "Global")
print_ratios(train_df, "Train")
print_ratios(valid_df, "Valid")
print_ratios(test_df, "Test")


# ------------------ saved datasets

name = arguments.output_datasets
name = name.replace("_XX", "")

train_df.to_csv(name.replace(".csv", "_train.csv"))
valid_df.to_csv(name.replace(".csv", "_valid.csv"))             
test_df.to_csv(name.replace(".csv", "_test.csv"))