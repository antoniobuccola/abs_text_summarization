import argparse

import pandas as pd

from pprint import pprint
from glob import glob

parser = argparse.ArgumentParser(description = """ITA dataset merger""", epilog = "See you later")
                    
parser.add_argument('--dataset_directory',
                    type = str, 
                    default = None,
                    help = "Directory in which the italian dataset is stored", 
                    required = True)  
                    
parser.add_argument('--common_attribute',
                    type = str, 
                    default = None,
                    help = "A common string shared by the name of the csv files to be merged", 
                    required = True)      

parser.add_argument('--output_file',
                    type = str, 
                    default = "global_dataset.csv",
                    help = "Path to the merged dataset (default: ./data/merged_dataset.csv)")
                    
args = parser.parse_args()

data_dir = args.dataset_directory                     
common_attribute = args.common_attribute

list_of_files = glob(f"{data_dir}/*{common_attribute}.csv")

names = [name.replace(f"{data_dir}", "").replace(f"_{common_attribute}", "").replace(".csv", "")
         for name in list_of_files]

dataframes = []
for name, file in zip(names, list_of_files):
    
    df = pd.read_csv(file, index_col = 0)
    df["name"] = name
    dataframes.append(df)
    
data = pd.concat(dataframes, ignore_index = True)
data.to_csv(args.output_file)
