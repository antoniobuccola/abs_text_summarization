import os
import requests
import argparse

import pandas as pd

from time import sleep
from bs4 import BeautifulSoup
from itertools import accumulate
from datetime import datetime as ddd

from glob import glob

data_path = "."
filename = "open_text_and_summary"

parser = argparse.ArgumentParser(description = """Open dataset creation""", epilog = "See you later")
                    
parser.add_argument('--step',
                    type = int, 
                    default = 50,
                    help = "Step to save intermediate results (default: 50)", 
                    required = False)  

parser.add_argument('--last_page',
                    type = int, 
                    default = 3400,
                    help = "Last page to scrape (default: the very last)", 
                    required = False)
                    
args = parser.parse_args()

summaries = []
texts = []
keywords = []

step = args.step
max_page = args.last_page

for page_num in range(1, max_page + 1):
    
    sleep(0.25)
    if page_num % step == 0 or page_num == max_page:
        
        # print timestamp, save partial results and clear the lists
        
        df = pd.DataFrame.from_dict({"news_it" : texts,
                                     "summary_it" : summaries,
                                     "keywords_it" : keywords})   
        
        df.to_csv(f"{data_path}/{filename}_page_{page_num}.csv")
        
        summaries.clear()
        texts.clear()
        keywords.clear()
                
        print(f"Page {page_num} / {max_page} @ {ddd.now().strftime('%d-%m-%Y %H-%M-%S')}")
    
    open_url = f"https://www.open.online/page/{page_num}/"
    
    try:
        page = requests.get(open_url)
        page.raise_for_status()
    
        soup_page = BeautifulSoup(page.text, "html.parser")
        div = soup_page.find("div", {"class" : "home-newsdaily"})
    
        for link in [link.find("a").get("href") for link in div.find_all("h2", {"class" : "news__title"})]:
    
            try:
                article = requests.get(link)
                article.raise_for_status()
                
                soup_article = BeautifulSoup(article.text, "html.parser")
                
                summary = soup_article.find("blockquote").text.replace("\n", "")
                
                div = soup_article.find("div", {"class": "news__content article-body adv-single-target"})
                
                paragraphs = [par.text + " " for par in div.find_all("p")]
                text = list(accumulate(paragraphs))[-1][:-1]
                
                div = soup_article.find("div", {"class": "news__terms"})
                keyword_list = [kw.text + ", " for kw in div.find_all("a")]
                keyword_str = list(accumulate(keyword_list))[-1][:-1]
                
            except Exception as E:
                print(E)
                continue
     
            summaries.append(summary)
            texts.append(text)
            keywords.append(keyword_str)
            
    except Exception as E:
        print(E)

dfs = []
list_of_chunks = glob(f"{data_path}/{filename}_page_*")

for chunk_file in list_of_chunks:    
    df = pd.read_csv(chunk_file, index_col = 0)
    dfs.append(df)
    
data = pd.concat(dfs).reset_index(drop = True)
data.to_csv(f"{data_path}/complete_datasets/{filename}.csv")

for f in list_of_chunks:
    os.system(f"rm -rf {f}")