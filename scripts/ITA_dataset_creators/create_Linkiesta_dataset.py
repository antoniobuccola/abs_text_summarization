# launch this script from /users/antonio/abs_text_summarization

import os
import requests
import argparse

import pandas as pd

from time import sleep
from bs4 import BeautifulSoup
from itertools import accumulate
from datetime import datetime as ddd

from glob import glob

data_path = "../data/ATS/ITA_datasets"
filename = "linkiesta_text_and_summary"

parser = argparse.ArgumentParser(description = """Linkiesta dataset creation - launch this script from /users/antonio/abs_text_summarization""", 
                                 epilog = "See you later")
                    
parser.add_argument('--step',
                    type = int, 
                    default = 50,
                    help = "Step to save intermediate results (default: 50)", 
                    required = False)  

args = parser.parse_args()

step = args.step

chunk = 1

common_url = "https://www.linkiesta.it"

str_categories = """Politica
Italia
Esteri
Cultura
Economia"""
categories = [cat.lower() for cat in str_categories.split("\n")]

headers = {'User-Agent': 'The First Hunter 1.0', 'From': 'moonpresence@huntersdream.com'}

summaries = []
texts = []

links = []

for category in categories:
    
    print(f"Current category: {category} @ {ddd.now().strftime('%H-%M-%S   %d-%m-%y')}")
    try:
        topic_url = f"{common_url}/argomento/{category}/" 
        topic = requests.get(topic_url, headers)
        topic.raise_for_status()
        
        soup_topic = BeautifulSoup(topic.text, "html.parser")
    
        pages = [a.get("title") for a in soup_topic.find("div",  {"class" : "navigation"}).find_all("a")]
        pages = [p for p in pages if p is not None]
        max_page = max([int(page) for page in pages if page.isdecimal()])
        
    except Exception as E:
        print("1", E)
        continue
        
    for page_num in range(1, max_page + 1):
        
        try:
            page_url = f"{topic_url}page/{page_num}/"
        
            page = requests.get(page_url)
            page.raise_for_status()

            soup_page = BeautifulSoup(page.text, "html.parser")
                                            
        except Exception as E:
            print("2", E)
            continue        
            
        for art in soup_page.find_all("article"):
            
            sleep(0.4)
            link = art.find("a").get("href")
            
            if link in links:
                continue
            else:
                links.append(links)
                                    
            try:
                article = requests.get(link, headers = headers)
                article.raise_for_status()
            
                soup_article = BeautifulSoup(article.text, "html.parser")
                summary = soup_article.find("div", {"class" : "row width-normal article-header"}).find("h2").text
                
                body = soup_article.find("div", {"class": "article-content"})

                pars = [par.text for par in body.find_all("p")]
                paragraphs = [par + " " for par in pars if len(par) > 0]
                text = list(accumulate(paragraphs))[-1][:-1]
                
                summaries.append(summary)
                texts.append(text)
                
            except Exception as E:
                print("3", E)
                continue
            
            if len(summaries) % step == 0 or page_num == max_page:       
                
                df = pd.DataFrame.from_dict({"news_it" : texts, "summary_it" : summaries})         
                df.to_csv(f"{data_path}/{filename}_chunk_{chunk}.csv")
                
                print(f"Chunk {chunk} @ {ddd.now().strftime('%d-%m-%Y %H-%M-%S')}")
                            
                summaries.clear()
                texts.clear()
                chunk += 1


dfs = []
list_of_chunks = glob(f"{data_path}/{filename}_chunk_*")

for chunk_file in list_of_chunks:    
    df = pd.read_csv(chunk_file, index_col = 0)
    dfs.append(df)
    
data = pd.concat(dfs, ignore_index = True)

data.to_csv(f"{data_path}/complete_datasets/{filename}.csv")

for f in list_of_chunks:
    os.system(f"rm -rf {f}")            