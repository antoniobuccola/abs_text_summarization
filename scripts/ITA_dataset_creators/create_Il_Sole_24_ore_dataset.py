# launch this script from /users/antonio/abs_text_summarization

import os
import argparse
import requests

import pandas as pd

from time import sleep, time
from bs4 import BeautifulSoup
from itertools import accumulate
from datetime import datetime as ddd

from glob import glob

parser = argparse.ArgumentParser(description = """Il Sole 24 Ore  dataset creation - launch this script from /users/antonio/abs_text_summarization""", epilog = "See you later")
                    
parser.add_argument('--step',
                    type = int, 
                    default = 50,
                    help = "Step to save intermediate results (default: 50)", 
                    required = False)  

parser.add_argument('--last_page',
                    type = int, 
                    default = 225,
                    help = "Last page to scrape (default: the very last)", 
                    required = False)
                    
args = parser.parse_args()

data_path = "."
filename = "il_sole_24_ore_text_and_summary"

il_sole_24_ore = "https://www.ilsole24ore.com"

summaries = []
texts = []
keywords = []

step = args.step

max_page = args.last_page

for page_num in range(1, max_page + 1):

    sleep(0.25)
    if page_num % step == 0 or page_num == max_page:
        
        # print timestamp, save partial results and clear the lists
        
        df = pd.DataFrame.from_dict({"news_it" : texts, 
                                     "summary_it" : summaries,
                                     "keywords_it" : keywords})   
        
        df.to_csv(f"{data_path}/{filename}_page_{page_num}.csv")
        
        summaries.clear()
        texts.clear()
        keywords.clear()
                
        print(f"Page {page_num} / {max_page} @ {ddd.now().strftime('%d-%m-%Y %H-%M-%S')}")
    
    sole_url = f"{il_sole_24_ore}/archivi/{page_num}/"
    
    try:
        page = requests.get(sole_url)
        page.raise_for_status()
        
        soup_page = BeautifulSoup(page.text, "html.parser")
        ol = soup_page.find("ol", {"class" : "list-lined list-lined--sep bytime"})
    
        for link in [il_sole_24_ore + link.find("a").get("href") 
                     for link in ol.find_all("h3", {"class" : "aprev-title"})]:           
            try:
                article = requests.get(link)
                article.raise_for_status()              
                
                soup_article = BeautifulSoup(article.text, "html.parser")       
                
                title = soup_article.find("h1", {"class" : "atitle"}).text
                subtitle = soup_article.find("h2", {"class" : "asummary d-none d-lg-block"}).text
                summary = title + ". " + subtitle
                
                paragraphs = [par.text + " " for par in soup_article.find_all("p", {"class" : "atext"})]
                text = list(accumulate(paragraphs))[-1][:-1]
                
                kws = [kw.text + ", " for kw in soup_article.find("ul", {"class": "afoot-args list-inline"}).find_all("a")]
                keyword_str = list(accumulate(kws))[-1][:-1]
                
                summaries.append(summary)
                texts.append(text)
                keywords.append(keyword_str)
        
            except Exception as E:                                     
                print(E)
                continue    
        
    except Exception as E:
        print(E)                             
        continue

dfs = []
list_of_chunks = glob(f"{data_path}/{filename}_page_*")

for chunk_file in list_of_chunks:    
    df = pd.read_csv(chunk_file, index_col = 0)
    dfs.append(df)
    
data = pd.concat(dfs).reset_index(drop = True)
data.to_csv(f"{data_path}/complete_datasets/{filename}.csv")

for f in list_of_chunks: os.system(f"rm -rf {f}")