# launch this script from /users/antonio/abs_text_summarization

import os
import requests
import argparse

import pandas as pd

from glob import glob
from time import sleep
from bs4 import BeautifulSoup
from itertools import accumulate
from datetime import datetime as ddd

common_url = "https://www.ilpost.it/"
str_categories = """Italia
Mondo
Politica
Tecnologia
Internet
Scienza
Cultura
Economia
Europa
Sport
Media
Moda
Libri
Consumismi"""
categories = [cat.lower() for cat in str_categories.split("\n")]

data_path = "."
filename = "il_post_text_and_summary_NEW"

parser = argparse.ArgumentParser(description = """Il Post dataset creation - launch this script from /users/antonio/abs_text_summarization""", 
                                 epilog = "See you later")
                    
parser.add_argument('--step',
                    type = int, 
                    default = 50,
                    help = "Step to save intermediate results (default: 50)", 
                    required = False) 

args = parser.parse_args()

summaries = []
texts = []
keywords = []

step = args.step

chunk = 1

for category in categories:

    try:
        topic_url = f"{common_url}/{category}" 
        topic = requests.get(topic_url)
        topic.raise_for_status()
        soup_topic = BeautifulSoup(topic.text, "html.parser")
    
        pages = [page_index.text.replace(".", "") 
                 for page_index in soup_topic.find_all("a", {"class": "page-numbers"})]
    
        max_page = max([int(page) for page in pages if page.isdecimal()])
    except Exception as E:
        print(E)
        continue
    
    for page_num in range(1, max_page + 1):
        
        try:
            page_url = f"{topic_url}/page/{page_num}"
        
            page = requests.get(page_url)
            page.raise_for_status()

            soup_page = BeautifulSoup(page.text, "html.parser")
        except Exception as E:
            print(E)
            continue    
            
        for h2 in soup_page.find_all("h2", {"class" : "entry-title"}):
            
            sleep(0.2)
            
            link = h2.find("a").get("href")
            
            try:
                article = requests.get(link)
                article.raise_for_status()
            
                soup_article = BeautifulSoup(article.text, "html.parser")
                
                title = soup_article.find("h1", {"class" : "entry-title"}).text
                subtitle = soup_article.find("h2", {"class" : "tit2"}).text
                summary = title + ". " + subtitle
                
                body = soup_article.find("div", {"id": "singleBody"})
                pars = [par.text for par in body.find_all("p")]
                
                paragraphs = [par + " " for par in pars if len(par) > 0 and "Leggi anche" not in par]
                text = list(accumulate(paragraphs))[-1][:-1]
                
                kws = [kw.text + ", " for kw in soup_article.find("div", {"class": "art_tag"}).find_all("a")]
                keyword_str = list(accumulate(kws))[-1][:-1]
                                               
                summaries.append(summary)
                texts.append(text)
                keywords.append(keyword_str)
                
            except Exception as E:
                print(E)
                continue
            
            if len(summaries) % step == 0 or page_num == max_page:       
                
                df = pd.DataFrame.from_dict({"news_it" : texts, 
                                             "summary_it" : summaries,
                                             "keywords_it" : keywords}) 
                
                df.to_csv(f"{data_path}/{filename}_chunk_{chunk}.csv")
                
                print(f"Chunk {chunk} @ {ddd.now().strftime('%d-%m-%Y %H-%M-%S')}")
                            
                summaries.clear()
                texts.clear()
                keywords.clear()
                chunk += 1

dfs = []
list_of_chunks = glob(f"{data_path}/{filename}_chunk_*")

for chunk_file in list_of_chunks:    
    df = pd.read_csv(chunk_file, index_col = 0)
    dfs.append(df)
    
data = pd.concat(dfs).reset_index(drop = True)
data.to_csv(f"{data_path}/complete_datasets/{filename}.csv")

for f in list_of_chunks:
    os.system(f"rm -rf {f}")