import os
import requests
import argparse

import pandas as pd

from time import sleep
from bs4 import BeautifulSoup
from itertools import accumulate
from datetime import datetime as ddd

from glob import glob

data_path = "."
filename = "il_fatto_quotidiano_text_and_summary"

parser = argparse.ArgumentParser(description = """Il Fatto quotidiano dataset creation""", epilog = "See you later")
                    
parser.add_argument('--step',
                    type = int, 
                    default = 50,
                    help = "Step to save intermediate results (default: 50)", 
                    required = False) 

args = parser.parse_args()

s = """
Elezioni 2021
Palazzi & Potere
Politica & Palazzo
Giustizia & ImpunitÃƒ 
Media & Regime
Economia & Lobby
Zona Euro
Lavoro & Precari
Cronaca
Cronaca Nera
Mafie
Mondo
Ambiente & Veleni
Sport & Miliardi
Scuola
Diritti
Referendum Eutanasia Legale
Cervelli fuga
SocietÃƒ 
Tecnologia
Scienza
Motori 2 0
Immobiliare"""

categories = [cat.replace(" & ", "-").replace(" ", "-").replace("Ãƒ ", "a").lower() for cat in s.split("\n")]

fatto_quotidiano_url = "https://www.ilfattoquotidiano.it"

parser = argparse.ArgumentParser(description = """Open dataset creation - launch this script from /users/antonio/abs_text_summarization""", 
                                 epilog = "See you later")
                    
parser.add_argument('--step',
                    type = int, 
                    default = 50,
                    help = "Step to save intermediate results (default: 50)", 
                    required = False)  

texts = []
summaries = []
keywords = []

chunk = 0
step = args.step

for category in categories:
    
    page_num = 1
    link_check = True
    
    common_url = f"{fatto_quotidiano_url}/{category}/" # page/2/   
    
    if link_check:
        pass
    else:
        # go to the next category
        continue
    
    while True:
              
        # loop on the category until the list of links is empty

        sleep(0.2)
        
        try:
            page_url = common_url + f"page/{page_num}/"
            
            page = requests.get(page_url)
            page.raise_for_status()
                    
            soup_page = BeautifulSoup(page.text, "html.parser")
            links = [h3.find("a").get("href") for h3 in soup_page.find_all("h3", {"class" : "p-item"})]
            
            if len(links) == 0: 
                # last page is blank and no links are retrieved
                break
                
        except Exception as E:
            print(E)
            break
            
        else:
                       
            page_num += 1
            
            for link in links:
                
                sleep(0.2)
                
                if len(summaries) % step == 0:
                    df = pd.DataFrame.from_dict({"news_it" : texts, 
                                                 "summary_it" : summaries,
                                                 "keyword_it" : keywords})    
                    
                    df.to_csv(f"{data_path}/{filename}_chunk_{chunk}.csv")
                
                    print(f"Chunk {chunk} @ {ddd.now().strftime('%d-%m-%Y %H-%M-%S')}")
                            
                    summaries.clear()
                    texts.clear()
                    keywords.clear()
                    chunk += 1
                
                try:
                    article = requests.get(link)
                    article.raise_for_status()
                
                    soup_article = BeautifulSoup(article.text, "html.parser")
                    
                    title = soup_article.find("h1", {"class" : "title-article"}).text
                    subtitle = soup_article.find("section", {"class" : "catenaccio"}).text
                    summary = title + ". " + subtitle
                    
                    section_pars = soup_article.find("section", {"class" : "article-content"})
                    paragraphs = [par.text + " " for par in section_pars.find_all("p")]
                    text = list(accumulate(paragraphs))[-1][:-1]
                    
                    kws = [kw.text + ", " for kw in soup_article.find("div", {"class": "more-info-tag"}).find_all("a")]
                    keyword_str = list(accumulate(kws))[-1][:-1]
                    
                    summaries.append(summary)
                    texts.append(text)
                    keywords.append(keyword_str)
                    
                except Exception as E:
                    continue

# save remaining articles                    
if len(texts) > 0:       
    
    df = pd.DataFrame.from_dict({"news_it" : texts, 
                                 "summary_it" : summaries,
                                 "keyword_it" : keywords})   
    
    df.to_csv(f"{data_path}/{filename}_chunk_{chunk}.csv")        
    
    print(f"Chunk {chunk} (final), length {df.shape[0]} saved @ {ddd.now().strftime('%d-%m-%Y %H-%M-%S')}")                    

    dfs = []
list_of_chunks = glob(f"{data_path}/{filename}_chunk_*")

for chunk_file in list_of_chunks:    
    df = pd.read_csv(chunk_file, index_col = 0)
    dfs.append(df)
    
data = pd.concat(dfs).reset_index(drop = True)
data.to_csv(f"{data_path}/complete_datasets/{filename}.csv")

for f in list_of_chunks:
    os.system(f"rm -rf {f}")