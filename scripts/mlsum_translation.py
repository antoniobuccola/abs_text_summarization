import warnings
warnings.filterwarnings("ignore")

import numpy as np
import pandas as pd

from time import time, sleep
from datasets import load_dataset
from googletrans import Translator

from datetime import datetime as d

tr = Translator()

def xx_it_translator(source_lang, source_text):
    
    sleep(0.4)
    
    try:
        trans_text = tr.translate(source_text, src = source_lang, dest = 'it').text
        return trans_text
    
    except Exception as E:
        
        print()
        print(source_text)
        print()
        print(E)
        
        return " "
    
def apply_mask(df_lang, lang):
    
    if lang == "fr":
        
        mask_1 = (df_lang['len_news'] >= 1000) & (df_lang['len_news'] <= 4000)
        mask_2 = (df_lang['len_summary'] >= 100) & (df_lang['len_summary'] <= 250)
        mask_3 = (df_lang['ratio'] >= 0.02) & (df_lang['ratio'] <= 0.08)
          
    elif lang == "es":

        mask_1 = (df_lang['len_news'] >= 1000) & (df_lang['len_news'] <= 5000)
        mask_2 = (df_lang['len_summary'] >= 100) & (df_lang['len_summary'] <= 200)
        mask_3 = (df_lang['ratio'] >= 0.02) & (df_lang['ratio'] <= 0.05)
    
    elif lang == "de":
        
        mask_1 = (df_lang['len_news'] >= 1000) & (df_lang['len_news'] <= 4500)
        mask_2 = (df_lang['len_summary'] >= 100) & (df_lang['len_summary'] <= 300)
        mask_3 = (df_lang['ratio'] >= 0.02) & (df_lang['ratio'] <= 0.09)
    
    return mask_1 & mask_2 & mask_3
    
###############################################################################################################################

import argparse   

parser = argparse.ArgumentParser(description = """MLSum translation with Google Translate Python API""", 
                                 epilog = "See you later")

parser.add_argument('language', 
                    type = str, 
                    default = None,  
                    help = "Chosen language (fr, es, de)")

parser.add_argument('--max_articles', 
                    type = int, 
                    default = -1,  
                    help = "Number of articles to be translated (default: all)")

parser.add_argument('--number_of_chunks', 
                    type = int, 
                    default = 2000,  
                    help = "Number of chunks to be saved (allows graduality, default: 2000)")


parser.add_argument('--save_path', 
                    type = str, 
                    default = "data/chunked_translation",  
                    help = "Path to directory where chunks have to be saved (default: data/chunked_translation)")

args = parser.parse_args()

max_articles = args.max_articles
lang = args.language

assert lang in ['fr', 'de', 'es'], "Possible languages are fr, de, es" 
    
dataset = load_dataset("mlsum", lang)
    
news = []
sums = []
    
for label in ["train", "validation", "test"]: # the splits the dataset is composed of  
        
    for dict_ in dataset[label]:         
            
        news.append(dict_['text'])
        sums.append(dict_['summary'])   
            
        news_col = f"news_{lang}"
        summ_col = f"summary_{lang}"   
            
df_lang = pd.DataFrame(list(zip(news, sums)), columns = [news_col, summ_col])   
    
df_lang['len_news'] = df_lang[news_col].apply(len)
df_lang['len_summary'] = df_lang[summ_col].apply(len)
df_lang['ratio'] = df_lang['len_summary'] / df_lang['len_news']
    
print(df_lang.describe())

try:
    sample = df_lang[apply_mask(df_lang, lang)][:max_articles]
    
except Exception as E:    
    sample = df_lang

print(f"original dataset: {df_lang.shape[0]} articles, filtered dataset: {sample.shape[0]} articles")

chunks = args.number_of_chunks
for index, chunk in enumerate(np.array_split(sample, chunks)):
    
    print()
    print(f"********** Processing chunk {index+1}, size {len(chunk)} @ ", d.now().strftime("%H:%M:%S / %d-%m-%Y"))
    print()
    
    chunk['news_it'] = chunk[f'news_{lang}'].apply(lambda text: xx_it_translator(source_lang = lang, source_text = text))
    chunk['summary_it'] = chunk[f'summary_{lang}'].apply(lambda text: xx_it_translator(source_lang = lang, source_text = text))
    
    chunk_name = f"mlsum_{lang}_it_articles_chunk_{index+1}.csv"
    chunk.to_csv(f"{args.save_path}/{chunk_name}")
