import pandas as pd

import argparse

parser = argparse.ArgumentParser(description = """Dataset train/valid/test splitter""", epilog = "See you later")

parser.add_argument('--input_dataset',
                    type = str, 
                    default = "data/merged_dataset.csv",
                    help = "Path in which the merged dataset will be stored after merging (default: data/merged_dataset.csv)")

parser.add_argument('--train_fraction',
                    type = float, 
                    default = 0.7,
                    help = "Fraction of the dataset for train / validation / test split (default: 0.7). Validation and test datasets are splitted by half with the examples not used for training.")

parser.add_argument('--output_datasets',
                    type = str, 
                    default = "data/dataset_for_XX.csv",
                    help = "Path in which the output csv files will be stored after merging and splitting (default: data/dataset_for_XX.csv, where XX = train, valid, test. If you give a different name, train / valid / test labels will be added to name)")

arguments = parser.parse_args()

# ------------------ load the merged dataset

articles = pd.read_csv(arguments.input_dataset, index_col = 0)

# train fraction will be user later for train / valid / test split
# if it is not correct, however, there is no need in continuing

train_fraction = arguments.train_fraction
assert train_fraction < 1, "Train fraction must be less than 1"

# ------------------ train / validation / test split

n_articles = articles.shape[0]

valid_fraction = (1 - train_fraction)/2

train_articles = int(n_articles*train_fraction)
train = articles[:train_articles]

valid_articles = int(n_articles*valid_fraction)
valid = articles[train_articles: train_articles + valid_articles]

test  = articles[train_articles + valid_articles:]

# ------------------ saved cleaned datasets

name = arguments.output_datasets
name = name.replace("_XX", "")

train.to_csv(name.replace(".csv", "_train.csv"))
valid.to_csv(name.replace(".csv", "_valid.csv"))             
test.to_csv(name.replace(".csv", "_test.csv"))